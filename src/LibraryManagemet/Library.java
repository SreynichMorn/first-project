package LibraryManagemet;
import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.CellStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;

import java.io.IOException;
import java.text.DateFormat;
import java.util.*;
public class Library {

    public static void main(String[] args) throws IOException {
        Library li = new Library();
        Book book[] = new Book[2];
        String liName = "";
        String liAddress = "";
        int op;
        int count = 0;
        int i = 0;
        Scanner obj = new Scanner(System.in);
        System.out.println("=============SET UP LIBRARY=============");
        System.out.print("===> Enter Library's Name:");
        liName = obj.nextLine();
        System.out.print("===> Enter Library's Address:");
        liAddress = obj.nextLine();
        //        System.out.printf(liName.toUpperCase()+" Library is already created in  "+liAddress.toUpperCase()+" Address successfully on");
        DateFormat DFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, Locale.getDefault());
        // Initializing the calender Object
        Calendar cal = Calendar.getInstance();
        System.out.println("'" + liName.toUpperCase() + "' Library is already created in  '" + liAddress.toUpperCase() + "' Address successfully on " + cal.getTime());

        do {
            System.out.println("|=========================" + liName.toUpperCase() + "," + liAddress.toUpperCase() + "========================|");
            System.out.println("|                            1- Add Book                             |");
            System.out.println("|                            2- Show All Books                       |");
            System.out.println("|                            3- Show Available Books                 |");
            System.out.println("|                            4- Borrow Book                          |");
            System.out.println("|                            5- Return Book                          |");
            System.out.println("|                            6- Exit                                 |");
            System.out.println("|--------------------------------------------------------------------|\n");
            System.out.print("Choose Option (1-6) :");
            op = obj.nextInt();
            switch (op) {
                case 1: {
                    System.out.println("=========ADD BOOK INFO=========\n");
                    li.addBook(book);


                }
                break;
                case 2: {
                    System.out.println("=========SHOW ALL BOOK INFO==========\n");
                    li.showAllBook(book);
                    System.out.println("Press 'Enter' to Continue !");
                }
                break;
                case 3: {
                    System.out.println("========= AVAILABLE BOOKS INFO =========\n");
                    li.showAvailable(book);
                    System.out.println("Press 'Enter' to Continue !");
                }
                break;
                case 4: {
                    System.out.println("=========BORROW BOOK INFO=========\n");
                    li.borrowBook(book);
                }break;
                case 5: {
                    System.out.println("=========RETURN BOOK INFO=========\n");
                    li.returnBook(book);
                }
                break;

                case 6: {
                    System.err.println("(^-^) Good Bye! (^-^)");
                    System.exit(0);
                }
                break;

            }
           // System.out.println("Press 'Enter' to Continue !");
            System.in.read();
        } while (op != 6);
    }

//    ===========Add Book============
    Scanner obj = new Scanner(System.in);
    int count=0;

    public void addBook(Book theBooks[]) {
      //  int count = 0;
        int id = count + 1;
        int a=0;
        System.out.println("Input ID:" + (count + 1));
        System.out.print("Enter Book's Name:");
        String title = obj.next();
        System.out.print("Enter Author's Name:");
        String author = obj.next();
        System.out.print("Enter Published Year:");
        int publishedYear = obj.nextInt();
        String status = "Available";
        if (count < 2) {
            theBooks[id - 1] = new Book(id, title, author, publishedYear, status);
            theBooks[id - 1].setId(id);
            a = 1;
            count++;
            System.err.print("Book is add successfully\n");
        }
        if(a==0){
            System.err.println("No space for add more book\n");
        }
    }


//========Show All Book=============
    void showAllBook(Book theBook[]) {
        CellStyle numberstyle=new CellStyle(CellStyle.HorizontalAlign.center);
        Table t=new Table(5, BorderStyle.UNICODE_ROUND_BOX, ShownBorders.ALL);
        t.setColumnWidth(0,8,14);
        t.setColumnWidth(1,7,16);
        t.setColumnWidth(2,9,16);
        t.addCell("ID",numberstyle);
        t.addCell("Title",numberstyle);
        t.addCell("Author",numberstyle);
        t.addCell("Publish Date",numberstyle);
        t.addCell("Status",numberstyle);

        if(count==0){
            System.err.println("No Book available\n");
        }
        else{
            for(int i=0;i<count;i++){
                t.addCell(theBook[i].getId()+"",numberstyle);
                t.addCell(theBook[i].getTitle(),numberstyle);
                t.addCell(theBook[i].getAuthor(),numberstyle);
                t.addCell(theBook[i].getPublishedYear()+"",numberstyle);
                t.addCell(theBook[i].getStatus(),numberstyle);
            }
            System.out.println(t.render());
        }
    }

//  ===============  Available Book===========
    void showAvailable(Book theBook[]){

        CellStyle numberstyle=new CellStyle(CellStyle.HorizontalAlign.center);
        Table t=new Table(5, BorderStyle.UNICODE_ROUND_BOX, ShownBorders.ALL);
        t.setColumnWidth(0,8,14);
        t.setColumnWidth(1,7,16);
        t.setColumnWidth(2,9,16);
        t.addCell("ID",numberstyle);
        t.addCell("Title",numberstyle);
        t.addCell("Author",numberstyle);
        t.addCell("Publish Date",numberstyle);
        t.addCell("Status",numberstyle);

        if(count==0){
            System.err.println("No Book available\n");
        }
        else{
            for(int i=0;i<count;i++){
                if (theBook[i] != null && theBook[i].getStatus().equalsIgnoreCase("Available")) {
                    t.addCell(theBook[i].getId() + "", numberstyle);
                    t.addCell(theBook[i].getTitle(), numberstyle);
                    t.addCell(theBook[i].getAuthor(), numberstyle);
                    t.addCell(theBook[i].getPublishedYear() + "",numberstyle);
                    t.addCell(theBook[i].getStatus(), numberstyle);
                }
            }
            System.out.println(t.render());
        }
    }


//   ================ Borrow Book===========
    void borrowBook(Book theBooks[]){
        int i=0;boolean num=false;
        System.out.print("Input ID for Borrow Book:");

        int bId=obj.nextInt();
        if(count==0){
            System.err.println("Book ID:" + bId + " not Exist...\n");
        }
        else {
            for (i = 0; i < theBooks.length; i++) {
                if(theBooks[i]!=null){
                    if (bId == theBooks[i].getId() && theBooks[i].getStatus().equalsIgnoreCase("Available")) {
                        bId = i;
                        System.out.println("ID:" + theBooks[i].getId());
                        System.out.println("Title:" + theBooks[i].getTitle());
                        System.out.println("Author:" + theBooks[i].getAuthor());
                        System.out.print("Published Year:" + theBooks[i].getPublishedYear());
                        theBooks[i].setStatus("Unavailable");
                        System.err.print(" is Borrowed Successfully!\n");
                        return;
                     }
                    else {
                        num = true;
                    }

                }
            }
        }
        if (num){
            System.err.println("Book ID:" + bId + " not Exist...\n");
        }
    }
//    =======Return Book=======
    void returnBook( Book theBooks[]){
        int i=0;boolean num=false;
        System.out.print("Input ID for Return Book:");
        int bId=obj.nextInt();
        if(count==0) {
            System.err.println("Book ID:"+bId+" is failed to return...\n");
        }
        else {
            for (i = 0; i < theBooks.length; i++) {
                if(theBooks[i]!=null){
                    if (bId == theBooks[i].getId()&&theBooks[i].getStatus().equalsIgnoreCase("Unavailable")) {
                        bId = i;
                        System.out.println("ID:" + theBooks[i].getId());
                        System.out.println("Title:" + theBooks[i].getTitle());
                        System.out.println("Author:" + theBooks[i].getAuthor());
                        System.out.print("Published Year:" + theBooks[i].getPublishedYear());
                        System.err.print(" is Returned Successfully!\n");
                        theBooks[i].setStatus("Available");
                        return;
                    }
                    else {
                        num=true;
                    }
                }
            }
        }
        if(num){
            System.err.println("Book ID:"+bId+" is failed to return...\n");
        }
    }
}







